// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "Unreal_22_4.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Unreal_22_4, "Unreal_22_4" );

DEFINE_LOG_CATEGORY(LogUnreal_22_4)
 